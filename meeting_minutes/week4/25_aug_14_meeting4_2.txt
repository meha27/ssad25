	            Minutes of meeting "25 august 2014"
			     Manu Jain
			  26 august 2014

        Contents
	1.Date and time
	2.Attendees
	3.Agenda
	4.Minutes
	5.Date of the next meeting.

1 Date and time
  25 august 2014 at 6:30pm

2.Attendees
  a)Sri Harsha sir
  b)Manu Jain
  c)Karan Mangla
  d)Meha Kaushik
  e)Akhil Thakur

3.Agenda
  To discuss the flowchart of working of our android app.

4.Minutes
  About the previous meeting-
  He told us to build a flowchart of working of the app.

  6:30pm to 6:40pm
  We made a flowchart of the step by step functioning of app.
  We explained about our idea of the working of the app.

  6:40pm to 6:50pm
  Sri Harsha sir made some changes in our flowchart i.e he said for the time being keep the app simple.
  He agreed to all our flowchart steps.

  6:50pm to 7:05pm
  He gave us motivation by asking to think tat you're making a app which will see a million downloads.
  Then he asked about the disadvantages of app and why people should use it.
  We also asked some queries i.e how users recieve voice invitations and how calls are made to invites simultaneously.

5.Date of the next meeting
  26 august 2014. 
  